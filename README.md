# Assessment Project - Java, Spring MVC, AngularJS

This is the solution for the Tipico Software Development Assessment Project - Java, Spring MVC, AngularJS

## Deployment
The project uses maven and grunt as the build system; grunt is called through maven.  
    
If needed change the `url.properties` and `database.properties` which can be found in `src/main/resources`

Finally build the project using: 

	mvn clean install

## Technologies

The project was built using Java JDK 1.8, Spring MVC, [spring-data](http://projects.spring.io/spring-data/) and [spring-data-rest](http://projects.spring.io/spring-data-rest/) and [AngularJS](https://angularjs.org/).

The front scaffold was generated using [yo angular generator](https://github.com/yeoman/generator-angular) version 0.11.1.


## Project Requirements

Create a new site where users are able to:

1. Create notes via a web based page having the ability to create a new note through an input field.
2. Notes can be submitted to the server by clicking a button "Send" which does its work by utilizing AngularJS.
3. All created notes are to displayed below the input field, sorted by date.
4. Notes can be sorted ascending or descending by its creation date.
5. Notes have a max length of 100 characters.
6. Notes must be saved on the server (mySQL DB, schema titled TIPICO) so that they can survive a reboot.
7. Unit tests should be available to test saving and loading notes as well as any other logic you see worth testing (Java and Angular).
8. The procedure of saving and loading a note should take place in a spring bean (eg. a service bean).
9. The list of notes below the input field should refresh to reflect changes.
10. The list of created notes should be pageable, showing 10 notes at a time, with the ability to view other pages in case there are more than 10 notes.

## Screenshots
There are some screen shots in the images folder.  