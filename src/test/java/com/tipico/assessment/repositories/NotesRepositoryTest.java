package com.tipico.assessment.repositories;

import com.tipico.assessment.bo.Note;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class NotesRepositoryTest {

    @Autowired
    NotesRepository noteRepository;

    private Note createNote(String text) {
        Note note = new Note();
        note.setText(text);
        return note;
    }

    private List<Note> generateNotes(int size) {
        return IntStream.rangeClosed(1, size).mapToObj((i) -> createNote(String.valueOf(i))).collect(Collectors.toList());
    }


    @Test
    public void shouldCreateANoteWhenSaveIsCalled() {
        String text = "NotesRepositoryTest Content";
        Note note = createNote(text);
        assertNull(note.getId());
        note = noteRepository.save(note);
        assertNotNull(note.getId());
        assertNotNull(note.getCreatedDate());
        assertEquals(text, note.getText());
    }

    @Test
    public void shouldGetAllNotesWhenFindAllIsCalled() {
        List<Note> notes = generateNotes(10);
        notes.stream().forEach(noteRepository::save);
        List<Note> retrievedNotes = (List<Note>) noteRepository.findAll();
        assertNotNull(retrievedNotes);
        assertEquals(notes.size(), retrievedNotes.size());
        assertThat(notes, is(retrievedNotes));
    }

    @Test
    public void shouldGetFirstPageWith5Items() {
        List<Note> notes = generateNotes(10);
        notes.stream().forEach(noteRepository::save);
        Page<Note> page = noteRepository.findAll(new PageRequest(0, 5));
        List<Note> retrievedNotes = page.getContent();
        assertNotNull(retrievedNotes);
        assertEquals(5, retrievedNotes.size());
        assertThat(notes.subList(0, 5), is(retrievedNotes));
    }

    @Test
    public void shouldGetSecondPageWith5Items() {
        List<Note> notes = generateNotes(10);
        notes.stream().forEach(noteRepository::save);
        Page<Note> page = noteRepository.findAll(new PageRequest(1, 5));
        List<Note> retrievedNotes = page.getContent();
        assertNotNull(retrievedNotes);
        assertEquals(5, retrievedNotes.size());
        assertThat(notes.subList(5, 10), is(retrievedNotes));
    }

    @Test
    public void shouldSortInAscendingOrderWhenASortableIsPresent() {
        List<Note> notes = generateNotes(5);
        notes.forEach((n) -> {
            noteRepository.save(n);
            try {
                Thread.sleep(1000);
            } catch(InterruptedException ignored){

            }
        });
        notes.sort((a, b) -> a.getCreatedDate().compareTo(b.getCreatedDate()));

        List<Note> retrievedNotes = (List<Note>) noteRepository.findAll(new Sort(Sort.Direction.ASC, "createdDate"));
        assertNotNull(retrievedNotes);
        assertEquals(notes.size(), retrievedNotes.size());
        assertThat(notes, is(retrievedNotes));
    }

    @Test
    public void shouldSortInDescendingOrderWhenASortableDescIsPresent() {
        List<Note> notes = generateNotes(5);
        notes.forEach((n) -> {
            noteRepository.save(n);
            try {
                Thread.sleep(1000);
            } catch(InterruptedException ignored){

            }
        });
        notes.sort((b, a) -> a.getCreatedDate().compareTo(b.getCreatedDate()));

        List<Note> retrievedNotes = (List<Note>) noteRepository.findAll(new Sort(Sort.Direction.DESC, "createdDate"));
        assertNotNull(retrievedNotes);
        assertEquals(notes.size(), retrievedNotes.size());
        assertThat(notes, is(retrievedNotes));
    }


}
