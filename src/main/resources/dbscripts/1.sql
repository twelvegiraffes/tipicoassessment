CREATE SCHEMA TIPICO;
USE TIPICO;

CREATE TABLE IF NOT EXISTS Note (
id bigint(20) NOT NULL AUTO_INCREMENT,
text varchar(100) NOT NULL,
createdDate datetime DEFAULT NULL,
PRIMARY KEY (id)
);