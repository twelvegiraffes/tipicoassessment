package com.tipico.assessment.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;

@Configuration
public class CustomRepositoryRestMvcConfiguration  extends RepositoryRestMvcConfiguration {

    public CustomRepositoryRestMvcConfiguration(){
        super();
    }

    @Override
    public HateoasPageableHandlerMethodArgumentResolver pageableResolver() {
        HateoasPageableHandlerMethodArgumentResolver resolver = super.pageableResolver();

        // Required since the default is a zero-based index and we need it to start from 1.
        resolver.setOneIndexedParameters(true);
        return resolver;
    }
}
