package com.tipico.assessment.repositories;

import com.tipico.assessment.bo.Note;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "notes", path = "notes")
public interface NotesRepository extends PagingAndSortingRepository<Note, Long> {

}
