package com.tipico.assessment.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SinglePageAppController {
    @RequestMapping("/")
    public String getView(){
        return "redirect:index.html";
    }
}
