package com.tipico.assessment.auditors;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

@Component
public class AuditorAwareImpl implements AuditorAware<Object> {

    @Override
    public String getCurrentAuditor() {
        return null;
    }


}