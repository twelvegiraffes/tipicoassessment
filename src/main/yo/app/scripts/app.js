'use strict';
angular.module('notesApp', [
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngTable'
]).config(function ($routeProvider, noteServiceProvider) {
    noteServiceProvider.baseUrl('http://localhost:8080/api/notes');
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
});
