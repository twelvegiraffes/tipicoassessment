'use strict';
angular.module('notesApp')
  .provider('noteService', function () {

    var baseUrl = null;

    function NoteService($http, $resource, $timeout, ngTableParams, baseUrl) {

      var notesRestAPI = $resource(baseUrl);

      /*jshint newcap: false */
      this.tableParams = new ngTableParams({
          page: 1,            // show first page
          count: 10,          // count per page
          size: 10,
          sorting: {
            createdDate: 'desc'     // initial sorting
          }
        },

        {
          counts: [],
          total: 0,           // length of data
          getData: function ($defer, params) {
            // ajax request to api
            var parameters = params.url();
            for (var p in parameters) {
              if (parameters.hasOwnProperty(p)) {
                if (p.indexOf('sorting') !== -1) {
                  var text = p.substring(p.indexOf('[') + 1, p.indexOf(']'));
                  var value = parameters[p];
                  parameters.sort = text + ',' + value;
                  delete parameters[p];
                }
              }
            }

            notesRestAPI.get(parameters, function (data) {
              $timeout(function () {
                // update table params
                params.total(data.page.totalElements);
                // set new data
                $defer.resolve(data._embedded.notes);
              }, 500);
            });
          }
        });
      /*jshint newcap: true */

      this.addNote = function (note) {
        var _this = this;
        var data = {text: note};
        $http.post(baseUrl, data).
          success(function () {
            _this.tableParams.reload();
          }).
          error(function (data) {
            console.log('Error', data);
          });
      };
    }

    this.baseUrl = function (b) {
      baseUrl = b;
    };

    // Method for instantiating
    this.$get = function ($http, $resource, $timeout, ngTableParams) {
      return new NoteService($http, $resource, $timeout, ngTableParams, baseUrl);
    };
  });
