'use strict';
angular.module('notesApp').controller('MainCtrl', function ($scope, noteService) {
  $scope.message = null;

  $scope.addNote = function () {
    if($scope.message !== null){
      noteService.addNote($scope.message);
      $scope.message = null;
    }
  };

  $scope.tableParams = noteService.tableParams;
});
