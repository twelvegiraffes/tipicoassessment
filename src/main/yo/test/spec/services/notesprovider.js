'use strict';

describe('Service: NotesProvider', function () {

  // load the service's module
  beforeEach(module('notesApp'));

  // instantiate service
  var scope, NotesProvider, httpMock;


  // Initialize the provider and a mock scope
  beforeEach(inject(function (noteService, $rootScope, $httpBackend) {
    scope = $rootScope.$new();
    NotesProvider = noteService;
    httpMock = $httpBackend;
  }));

  it('tests the providers is initialised', function(){
    expect(NotesProvider).not.toBeNull();
  });

  describe('Notes loading and save functionality', function(){
    it('should add note to current list if success is returned from backend', function(){

    });

    it('should not add note to current list if error is returned from backend', function(){

    });

    it('should returned a list of notes from the backend', function(){

    });

    it('should format url to match backend rest request', function(){

    });

    it('should sort notes in asc order', function(){

    });

    it('should sort notes in desc order', function(){

    });
  });

});
