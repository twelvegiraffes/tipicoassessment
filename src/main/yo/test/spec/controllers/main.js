'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('notesApp'));

  var MainCtrl, scope, NotesServiceMock;

  beforeEach(function() {
    NotesServiceMock = {
      tableParams : [],
      addNote: function(){}
    };
  });

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope,
      noteService: NotesServiceMock
    });
  }));

  describe('Add note functionality', function(){
    it('should contain a null message on startup', function(){
      expect(scope.message).toBeNull();
    });

    it('should call the addNote in the service with the current message.', function(){
      var currentMessage = 'Unit Test Message';
      scope.message = currentMessage;
      spyOn(NotesServiceMock, 'addNote').and.callThrough();

      scope.addNote();

      expect(NotesServiceMock.addNote).toHaveBeenCalledWith(currentMessage);
    });

    it('should have tableParams equal to what the service is creating.', function(){
      expect(scope.tableParams).toBe(NotesServiceMock.tableParams);
    });
  });
});
