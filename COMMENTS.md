# Assessment Project Comments And Assumptions

Please write down any comments or assumptions you would like us to know when reviewing the test in the list below.

## Comments And Assumptions

* The system uses JDK 1.8.  Maven might run into problems with setting JDK 1.8.  If that happens please refer [here](http://stackoverflow.com/questions/24705877/cant-get-maven-to-recognize-java-1-8)
* We did not use any connection pooling, it was beyond the scope of this project.
* The system uses [spring-data](http://projects.spring.io/spring-data/) and [spring-data-rest](http://projects.spring.io/spring-data-rest/).
* The front scaffold was generated using [yo angular generator](https://github.com/yeoman/generator-angular) version 0.11.1.
* Maven delegates the build to grunt so that the web resources are passed through the frontend build pipeline.
* In order to change the rest baseUrl change the file `url.properties` in `src/main/resources`
* In order to change the db connection settings change the file `database.properties` in `src/main/resources`

The project uses maven and grunt as the build system; grunt is called through maven.  To build the project use 

    mvn clean install
    
Do this after you change the `url.properties` and `database.properties` as outlined above.  